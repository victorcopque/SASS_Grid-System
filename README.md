> Agradecimentos: Mateus Cordeiro(@mateuscgc) pelas sugestões de melhoria e modularização do projeto

# _Mixin: grid-system()

```SCSS
@mixin grid-system($keys, $numsCols) {
    position: relative;
    float: left;
    @for $i from 1 to (length($numsCols) + 1) {
        @if(nth($keys, $i) == $xs){
            width: nth($numsCols, $i)*$colSize;
        }
        @else{
            @include device-width(nth($keys, $i)) {
                width: nth($numsCols, $i)*$colSize;
            }
        }
    }
}
```
* A função acima permite que usemos o grid-system do bootstrap de uma maneira mais enxuta e eficiente. A função é voltada para projetos mais simples. A mesma pode receber como parâmetros um ou mais valores para as colunas, exemplo:
```SCSS
@include grid-system($xs, 3); == "col-xs-3"
@include grid-system(($xs, $sm), (2, 4)); == "col-xs-2 col-sm-4"
```
* Podemos também definir resoluções que não estejam no escopo do bootstrap grid-system. Como resultado, você obterá uma media-query para a resolução definida:
```SCSS
@include grid-system((800px, 1600px), (3, 6));
```
* Isso permite que tenhamos um HTML mais limpo -- já que não é necessário adicionar diversas classes ao HTML -- ao passo que o efeito será o mesmo já que as características são implementadas no código em scss.

* O mesmo ocorre para o grid-system-offset:
```SCSS
@mixin grid-system-offset($keys, $numsCols) {
    @for $i from 1 to (length($numsCols) + 1) {
        @if(nth($keys, $i) == $xs){
            margin-left: nth($numsCols, $i)*$colSize;
        }
        @include device-width(nth($keys, $i)) {
            margin-left: nth($numsCols, $i)*$colSize;
        }
    }
}
```
* Para a classe row a mixin se torna ainda mais simples:
```SCSS
@mixin row() {
    margin-right: -15px;
    margin-left: -15px;
    &::before,
    &::after {
        display: table;
        content: " ";
    }
    &::after{
        clear: both;
    }
}
```
* Obs.: As mixins acima referenciam a mixin "device-width". Segue a descrição da mesma:
```SCSS
@mixin device-width($resolution, $variable: min-width){
    @media screen and ($variable: $resolution){
        @content;
    }
}
```
